DROP TABLE IF EXISTS anuncio_simple;
DROP TABLE IF EXISTS anuncio;

-- ----------------------------------------------------------------------------
-- Tablas
-- ----------------------------------------------------------------------------

CREATE TABLE anuncio (
    id BIGINT IDENTITY PRIMARY KEY,
    clave VARCHAR(255) NOT NULL,
    canal VARCHAR(50) NOT NULL,
    titulo VARCHAR(100) NOT NULL,
    tipo VARCHAR(50) NOT NULL,
    nivel VARCHAR(50) NOT NULL,
    cron VARCHAR(50),
    duracion_minutos INT,
    UNIQUE (clave, canal)
);

CREATE TABLE anuncio_simple (
    id BIGINT PRIMARY KEY REFERENCES anuncio(id),
    contenido1 VARCHAR(100),
    contenido2 VARCHAR(100),
    contenido3 VARCHAR(100)
);

-- -----------------------------------------------------------------------------
-- anuncios simples
-- -----------------------------------------------------------------------------
INSERT INTO anuncio
(id,    titulo,                 canal,      clave,          tipo,       nivel,      cron,           duracion_minutos) VALUES
(1,     'Bugs en prod',         'pnt',      'bugs',         'simple',   'ERROR',    '30 9 * * *',   30),
(2,     'OT pendientes',        'pnt',      'ot',           'simple',   'INFO',     null,           null),
(3,     'Mis Expensas',         'pnt',      'expensas',     'simple',   'INFO',     null,           null);

INSERT INTO anuncio_simple
(id,    contenido1,         contenido2,         contenido3) VALUES
(1,     'QC 3435',          'QC 1111',          'QC 3434'),
(2,     'OT 1299',          null,               null),
(3,     '2802 UF',          '$3950 / mes',      '103 edificios');
