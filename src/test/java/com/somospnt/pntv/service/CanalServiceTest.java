package com.somospnt.pntv.service;

import com.somospnt.pntv.ChetvAbstractTest;
import java.util.List;
import static org.junit.Assert.assertFalse;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class CanalServiceTest extends ChetvAbstractTest {

    @Autowired
    private CanalService canalService;

    @Test
    public void obtenerTodos_conCanalesExistentes_devuelveListaCanales() {
        List<String> canales = canalService.obtenerTodos();
        assertFalse(canales.isEmpty());
    }
}
