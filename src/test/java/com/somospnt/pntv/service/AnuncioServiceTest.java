package com.somospnt.pntv.service;

import com.somospnt.pntv.ChetvAbstractTest;
import com.somospnt.pntv.domain.Anuncio;
import com.somospnt.pntv.domain.AnuncioSimple;
import com.somospnt.pntv.domain.Nivel;
import java.util.List;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class AnuncioServiceTest extends ChetvAbstractTest {

    @Autowired
    private AnuncioService anuncioService;

    private AnuncioSimple crearAnuncioSimpleBasico() {
        AnuncioSimple anuncio = new AnuncioSimple();
        anuncio.setCanal("pnt");
        anuncio.setClave("prueba");
        anuncio.setTitulo("Un titulo del anuncio");
        anuncio.setNivel(Nivel.INFO);
        anuncio.setContenido1("contenido uno");
        return anuncio;
    }

    @Test
    public void guardar_anuncioSimpleValido_guardaElAnuncio() {
        AnuncioSimple anuncio = crearAnuncioSimpleBasico();
        anuncioService.guardar(anuncio);
        assertNotNull(anuncio.getId());
    }

    @Test
    public void guardar_anuncioSimpleSinNivel_guardaElAnuncioConNivelInfo() {
        AnuncioSimple anuncio = crearAnuncioSimpleBasico();
        anuncio.setNivel(null);
        anuncioService.guardar(anuncio);
        assertNotNull(anuncio.getId());
        assertEquals(Nivel.INFO, anuncio.getNivel());
    }

    @Test
    public void guardar_anuncioSimpleConHoraInicioYHoraFin_guardaElAnuncio() {
        AnuncioSimple anuncio = crearAnuncioSimpleBasico();
        anuncio.setCron("* * * * *");
        anuncio.setDuracionMinutos(10);;
        anuncioService.guardar(anuncio);
        assertNotNull(anuncio.getId());
    }

    @Test
    public void guardar_anuncioSimpleValidoSinClave_guardaElAnuncioConUnaClaveAutogenerada() {
        AnuncioSimple anuncio = crearAnuncioSimpleBasico();
        anuncio.setClave(null);
        anuncioService.guardar(anuncio);
        assertNotNull(anuncio.getId());
        assertNotNull(anuncio.getClave());
    }

    @Test
    public void actualizar_anuncioExistente_actualizaElAnuncio() {
        AnuncioSimple anuncio = crearAnuncioSimpleBasico();
        anuncio.setId(null);
        anuncio.setCanal("pnt");
        anuncio.setClave("bugs");
        anuncio.setTitulo("Titulo actualizado");
        anuncio.setContenido1("Contenido actualizado");

        anuncioService.actualizar(anuncio);

        assertNotNull(anuncio.getId());
    }

    @Test
    public void buscarPorCanal_canalExiste_devuelveAnunciosDelCanal() {
        String canal = "pnt";
        List<Anuncio> anuncios = anuncioService.buscarPorCanal(canal);
        assertFalse(anuncios.isEmpty());
        for (Anuncio anuncio : anuncios) {
            assertEquals(canal, anuncio.getCanal());
        }
    }

    @Test
    public void eliminar_canalYclaveExisten_borraElAnuncio() {
        String canal = "pnt";
        String clave = "bugs";
        int filasAnuncioAntes = jdbcTemplate.queryForObject("select count(*) from anuncio", Integer.class);
        int filasAnuncioSimpleAntes = jdbcTemplate.queryForObject("select count(*) from anuncio_simple", Integer.class);

        anuncioService.eliminar(canal, clave);
        entityManager.flush();

        int filasAnuncioDespues = jdbcTemplate.queryForObject("select count(*) from anuncio", Integer.class);
        int filasAnuncioSimpleDespues = jdbcTemplate.queryForObject("select count(*) from anuncio_simple", Integer.class);

        assertEquals(filasAnuncioAntes - 1, filasAnuncioDespues);
        assertEquals(filasAnuncioSimpleAntes - 1, filasAnuncioSimpleDespues);
    }
    
    @Test
    public void eliminar_canalYclaveNoExisten_noFalla() {
        String canal = "canal-no-existente";
        String clave = "bugs";
        int filasAnuncioAntes = jdbcTemplate.queryForObject("select count(*) from anuncio", Integer.class);
        int filasAnuncioSimpleAntes = jdbcTemplate.queryForObject("select count(*) from anuncio_simple", Integer.class);

        anuncioService.eliminar(canal, clave);
        entityManager.flush();

        int filasAnuncioDespues = jdbcTemplate.queryForObject("select count(*) from anuncio", Integer.class);
        int filasAnuncioSimpleDespues = jdbcTemplate.queryForObject("select count(*) from anuncio_simple", Integer.class);

        assertEquals(filasAnuncioAntes, filasAnuncioDespues);
        assertEquals(filasAnuncioSimpleAntes, filasAnuncioSimpleDespues);
    }

    @Test
    public void buscarPorCanalClave_conCanalYClaveExistente_devuelveAnuncio() {
        String canal = "pnt";
        String clave = "bugs";

        Anuncio anuncio = anuncioService.buscarPorCanalClave(canal, clave);

        assertNotNull(anuncio);
        assertEquals(clave, anuncio.getClave());
        assertEquals(canal, anuncio.getCanal());
    }

}
