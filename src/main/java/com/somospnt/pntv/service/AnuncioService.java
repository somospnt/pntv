package com.somospnt.pntv.service;

import com.somospnt.pntv.domain.Anuncio;
import java.util.List;

public interface AnuncioService {

    void guardar(Anuncio anuncio);

    void actualizar(Anuncio anuncio);

    List<Anuncio> buscarPorCanal(String canal);

    void eliminar(String canal, String clave);

    Anuncio buscarPorCanalClave(String canal, String clave);

}
