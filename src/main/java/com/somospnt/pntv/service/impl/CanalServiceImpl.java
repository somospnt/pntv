package com.somospnt.pntv.service.impl;

import com.somospnt.pntv.repository.AnuncioRepository;
import com.somospnt.pntv.service.CanalService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CanalServiceImpl implements CanalService {

    @Autowired
    private AnuncioRepository anuncioRepository;

    @Override
    public List<String> obtenerTodos() {
        return anuncioRepository.findAllCanales();
    }

}
