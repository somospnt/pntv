package com.somospnt.pntv.service.impl;

import com.somospnt.pntv.domain.Anuncio;
import com.somospnt.pntv.domain.Nivel;
import com.somospnt.pntv.repository.AnuncioRepository;
import com.somospnt.pntv.service.AnuncioService;
import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AnuncioServiceImpl implements AnuncioService {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private AnuncioRepository anuncioRepository;

    @Override
    public void guardar(Anuncio anuncio) {
        if (anuncio.getClave() == null) {
            anuncio.setClave(UUID.randomUUID().toString());
        }
        if (anuncio.getNivel() == null) {
            anuncio.setNivel(Nivel.INFO);
        }
        anuncioRepository.save(anuncio);
    }

    @Override
    public void actualizar(Anuncio anuncio) {
        Anuncio anuncioExistente = anuncioRepository.findByCanalAndClave(anuncio.getCanal(), anuncio.getClave());
        anuncio.setId(anuncioExistente.getId());
        entityManager.merge(anuncio);
    }

    @Override
    public List<Anuncio> buscarPorCanal(String canal) {
        return anuncioRepository.findByCanal(canal);
    }

    @Override
    public void eliminar(String canal, String clave) {
        Anuncio anuncio = anuncioRepository.findByCanalAndClave(canal, clave);
        if (anuncio != null) {
            anuncioRepository.delete(anuncio);
        }
    }

    @Override
    public Anuncio buscarPorCanalClave(String canal, String clave) {
        return anuncioRepository.findByCanalAndClave(canal, clave);
    }

}
