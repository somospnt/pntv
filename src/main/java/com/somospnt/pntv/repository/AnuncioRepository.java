package com.somospnt.pntv.repository;

import com.somospnt.pntv.domain.Anuncio;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnuncioRepository extends CrudRepository<Anuncio, Long> {

    List<Anuncio> findByCanal(String canal);

    Anuncio findByCanalAndClave(String canal, String clave);

    @Query("select distinct a.canal from Anuncio a")
    List<String> findAllCanales();
}
