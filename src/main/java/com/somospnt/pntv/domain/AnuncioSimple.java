package com.somospnt.pntv.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("simple")
public class AnuncioSimple extends Anuncio {

    private String contenido1;
    private String contenido2;
    private String contenido3;

    public String getContenido1() {
        return contenido1;
    }

    public void setContenido1(String contenido1) {
        this.contenido1 = contenido1;
    }

    public String getContenido2() {
        return contenido2;
    }

    public void setContenido2(String contenido2) {
        this.contenido2 = contenido2;
    }

    public String getContenido3() {
        return contenido3;
    }

    public void setContenido3(String contenido3) {
        this.contenido3 = contenido3;
    }
}
