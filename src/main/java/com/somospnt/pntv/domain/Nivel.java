package com.somospnt.pntv.domain;

public enum Nivel {
    INFO,
    EXITO,
    ALERTA,
    ERROR
}
