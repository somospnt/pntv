package com.somospnt.pntv.controller;

import com.somospnt.pntv.domain.Anuncio;
import com.somospnt.pntv.service.AnuncioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AnuncioRestController {

    @Autowired
    private AnuncioService anuncioService;

    @PutMapping("/api/canales/{canal}/anuncios")
    public Anuncio crear(@PathVariable String canal, @RequestBody Anuncio anuncio) {
        anuncio.setCanal(canal);
        Anuncio anuncioExistente = anuncioService.buscarPorCanalClave(anuncio.getCanal(), anuncio.getClave());
        if (anuncioExistente == null) {
            anuncioService.guardar(anuncio);
        } else {
            anuncioService.actualizar(anuncio);
        }
        return anuncio;
    }

    @GetMapping("/api/canales/{canal}/anuncios")
    public List<Anuncio> buscarTodos(@PathVariable String canal) {
        return anuncioService.buscarPorCanal(canal);
    }

    @DeleteMapping("/api/canales/{canal}/anuncios/{clave}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void eliminar(@PathVariable String canal, @PathVariable String clave) {
        anuncioService.eliminar(canal, clave);
    }

}
