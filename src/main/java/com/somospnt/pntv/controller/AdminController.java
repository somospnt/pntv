package com.somospnt.pntv.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AdminController {

    @RequestMapping("/canales/{canal}/admin")
    public String admin(@PathVariable String canal, Model model) {
        model.addAttribute("canal", canal);
        return "backend/admin";
    }

}
