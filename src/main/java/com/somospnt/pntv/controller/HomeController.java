package com.somospnt.pntv.controller;

import com.somospnt.pntv.service.CanalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    @Autowired
    private CanalService canalService;

    @RequestMapping("/")
    public String home(Model model) {
        model.addAttribute("canales", canalService.obtenerTodos());
        return "backend/canales";
    }

    @RequestMapping("/canales/{canal}")
    public String home(@PathVariable String canal, Model model) {
        model.addAttribute("canal", canal);
        return "home";
    }

    @RequestMapping("/login.html")
    public String login() {
        return "login";
    }
}
