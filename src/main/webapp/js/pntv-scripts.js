/*******************************************************************************
 *
 * Simplificacion del archivo scripts.js del template ORB.
 *
 ******************************************************************************/
"use strict";
$(document).ready(function() {

    // ========================================================================
    //	Togglers
    // ========================================================================

    // toogle sidebar
    $('.left-toggler').click(function(e) {
        $(".responsive-admin-menu").toggleClass("sidebar-toggle");
        $(".content-wrapper").toggleClass("main-content-toggle-left");
        e.preventDefault();
    });

    // We should listen touch elements of touch devices
    $('.smooth-overflow').on('touchstart', function(event) {
    });

    // toogle sidebar
    $('.right-toggler').click(function(e) {
        $(".main-wrap").toggleClass("userbar-toggle");
        e.preventDefault();
    });

    // ========================================================================
    //	Nanoscroller
    // ========================================================================

    if ($('.nano').length) {
        $(".nano").nanoScroller();
    }

    // ========================================================================
    //	Bootstrap Tooltips and Popovers
    // ========================================================================

    if ($('.tooltiped').length) {
        $('.tooltiped').tooltip();
    }

    // ========================================================================
    //	Left Responsive Menu
    // ========================================================================

    (function multiLevelAccordion($root) {

        var $accordions = $('.accordion', $root).add($root);
        $accordions.each(function() {

            var $this = $(this);
            var $active = $('> li > a.submenu.active', $this);
            $active.next('ul').css('display', 'block');
            $active.addClass('downarrow');
            var a = $active.attr('data-id') || '';

            var $links = $this.children('li').children('a.submenu');
            $links.click(function(e) {
                if (a !== "") {
                    $("#" + a).prev("a").removeClass("downarrow");
                    $("#" + a).slideUp("fast");
                }
                if (a == $(this).attr("data-id")) {
                    $("#" + $(this).attr("data-id")).slideUp("fast");
                    $(this).removeClass("downarrow");
                    a = "";
                } else {
                    $("#" + $(this).attr("data-id")).slideDown("fast");
                    a = $(this).attr("data-id");
                    $(this).addClass("downarrow");
                }
                e.preventDefault();
            });
        });
    })($('#menu'));




    // Responsive Menu Adding Opened Class//

    $(".responsive-admin-menu #menu li").hover(
            function() {
                $(this).addClass("opened").siblings("li").removeClass("opened");
            },
            function() {
                $(this).removeClass("opened");
            }
    );

});




/*******************************************************************************
 *
 * Servicios y utilidades generales para Che.
 *
 ******************************************************************************/
$(document).ready(function() {
    initAbrirMenu();
    initJsrender();
    initSolapas();
    initDatePicker();
    initIconPicker();
    initTimeago();
    initNotificaciones();
    initXEditable();
    initAusencias();
    initBuscadorGeneral();
    initBuscador();
    initInfoPersonasExperiencia();
    initHotkeys();
    initCargando();

    function initAbrirMenu() {
        $(".che-js-abrir-menu").click();
    }

    function initCargando() {
        $("#che-js-cargando").slideUp();
    }

    /**
     * Inicializo de esta manera todos los templates de JSRender para que queden disponibles para utilizar así: var html = $.render.myTmpl(myData, myHelpers);
     */
    function initJsrender() {
        $.templates("infoPersonas", "#infoPersonasTmpl");
        $.templates("popover", "#popoverTmpl");
        $.views.helpers({
            formatTimestamp: function (unixTimestamp, format) {
                return moment(unixTimestamp).format(format);
            }
        });
    }

    function initXEditable() {
        $.fn.combodate.defaults = {
            minYear: 1940,
            maxYear: new Date().getFullYear() + 1
        };
    }

    function initDatePicker() {
        $('.che-js-datepicker').datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 0,
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>'
        });
    }

    function initIconPicker() {
        $('button[role="che-iconpicker"]').iconpicker();
    }

    function initTimeago() {
        $(".che-js-timeago").each(function() {
            var $this = $(this);
            var timestamp = $this.data("timeagoTimestamp");
            var usarPrefijo = $this.data("timeagoQuitarPrefijo") | false;
            var timeago = moment(timestamp).fromNow(usarPrefijo);
            $this.html(timeago);
        });
    }

    function initNotificaciones() {
        var cantidadNotificaciones = 0;
        $(".che-js-notificacion").each(function() {
            var $this = $(this);
            var fechaVencimiento = $this.data("fechaVencimiento");
            var fechaActual = che.ui.fechaActual("YYYY-MM-DD");
            if (fechaVencimiento <= fechaActual) {
                $this.addClass("bg-danger");
            } else {
                $this.addClass("bg-info");
            }
            cantidadNotificaciones++;
        });

        $("#cantidadNotificaciones").text(cantidadNotificaciones);
        if (cantidadNotificaciones > 0) {
            $("#aviso-nueva-notificacion").removeClass("hide");
        }

        $(".che-js-notificacion-aceptar").click(function() {
            var $this = $(this);
            var idNotificacion = $this.closest(".che-js-notificacion").data("id-notificacion");
            che.service.notificacion.eliminar(idNotificacion)
                    .done(function() {
                        $this.closest(".che-js-notificacion").slideUp(250);
                        $("#cantidadNotificaciones").text(--cantidadNotificaciones);
                        if (cantidadNotificaciones === 0) {
                            $("#aviso-nueva-notificacion").addClass("hide");
                        }
                    });
            return false;
        });
    }

    function initSolapas() {
        //seleccionar tab activo
        $('#myTab a[href$="' + window.location.hash + '"]').tab('show');

        $(".che-js-tab").click(function() {
            var href = $(this).attr("href");
            window.location.hash = href.substring(href.indexOf('#'));
        });
    }

    function initAusencias() {
        initAcciones();
        initInfoAusencias();

        function initAcciones() {
            $('.che-js-accion-pedido-ausencia').click(actualizarPedidoAusencia);
            $('.che-js-accion-borrar-pedido-ausencia').click(eliminarPedidoAusencia);
        }

        function initInfoAusencias() {
            $("tr.che-js-fila-ausencia").each(function() {
                var $this = $(this);
                var dias = $this.data("diasAusencias").split(",");
                var codigoAusencia = $this.data("codigoAusencia");
                var urlPedidoAusencia = $this.data("urlPedidoAusencia");

                var html = "";
                var diasTexto, i;

                html += "<h5>Código Único:</h5>";
                html += "<small>" + codigoAusencia + "</small>";

                if (dias.length > 1) {
                    html += "<h5>" + dias.length + " días:</h5>";
                    diasTexto = "<small>" + dias[0].trim() + " al " + dias[dias.length - 1].trim() + "</small>";
                }
                else {
                    html += "<h5>1 día:</h5>";
                    diasTexto = "<small>" + dias[0].trim() + "</small>";
                }
                html += diasTexto;

                html += "<br><br><p class='text-right'><a href='" + urlPedidoAusencia + "'>Detalles <i class='fa fa-chevron-right'></i></a></p>";

                $this.find(".fa-stack").popover({
                    placement: "top",
                    title: "Detalle de la ausencia",
                    content: html,
                    container: "body",
                    html: true,
                    trigger: "click"
                }).addClass("che-cursor-help");
            });
        }

        function eliminarPedidoAusencia() {
            if (confirm("Vas a eliminar el pedido de ausencia. ¿Estás seguro?")) {
                var $this = $(this);
                $this.prop("disabled", true);
                var $pedido = $this.closest(".che-tabla-acciones");
                che.service.pedidoAusencia.eliminar($pedido.data('id-pedido'))
                        .done(function() {
                            $pedido.closest("tr").slideUp(400);
                        })
                        .fail(function() {
                            alert("Ocurrio un error procesando la operacion.");
                        });
                return false;
            }
        }

        function actualizarPedidoAusencia() {
            var $this = $(this);
            var idPedido = $this.closest(".che-tabla-acciones").data("id-pedido");
            var estado = $this.data("estado");
            var $botonesAccion = $this.closest(".che-js-fila-ausencia").find(".che-js-accion-pedido-ausencia");
            $botonesAccion.hide();

            che.service.pedidoAusencia.actualizarEstado(idPedido, estado)
                    .done(function(data) {
                        if ($this.hasClass("che-js-detalle-ausencia")) {
                            var $estado = $this.closest("h2").find(".che-js-estado-ausencia");
                        } else {
                            var $estado = $this.closest("tr").find(".che-js-estado-pedido-ausencia");
                        }
                        var $botonCerrar = $this.closest("tr").find(".che-js-accion-cerrar-pedido-ausencia");
                        $estado.html(estado);
                        $estado.removeClass("label-default label-success label-danger");
                        switch (estado) {
                            case "ACEPTADO":
                                $estado.addClass("label-success");
                                break;
                            case "RECHAZADO":
                                $estado.addClass("label-danger");
                                break;
                            case "CERRADO":
                                $estado.addClass("label-info");
                                $botonCerrar.css("visibility", "hidden");
                                break;
                            default:
                                $estado.addClass("label-default");
                        }
                    })
                    .fail(function() {
                        alert("Ocurrio un error procesando la operacion.");
                        $botonesAccion.show();
                    });
            return false;
        }
    }

    function initBuscadorGeneral() {
        var personasEmpresa = [];
        init();

        function init() {
            var sectorDefault = $("#listaPersonasDeEmpresa").data("sectorDefault");
            $("#listaPersonasDeEmpresa option").each(function() {
                var $this = $(this);
                var persona = {
                    label: $this.data("nombreApellido"),
                    nombreApellido: $this.data("nombreApellido"),
                    sector: $this.data("sector"),
                    cssImagenPerfil: $this.data("cssImagenPerfil"),
                    keywords: che.ui.reemplazarAcentos($this.data("keywords")).trim().toLowerCase(),
                    url: $this.data("url")
                };
                personasEmpresa.push(persona);
            });

            var $autocomplete = $("#che-buscador-personas-general").autocomplete({
                source: filtrarPersonas,
                select: function(event, ui) {
                    window.location = ui.item.url;
                    return false;
                }
            });
            $autocomplete.autocomplete("instance")._renderItem = renderItem;
            $autocomplete.focus(function() {
                var $this = $(this);
                if (!$this.val()) {
                    $this.val(sectorDefault.toLowerCase());
                }
                $this.autocomplete("search", $this.val());
            })
            $autocomplete.click(function() {
                $(this).select(); //seleccionar todo el texto
            });
        }

        function renderItem(ul, item) {
            return $("<li class='che-menu-item-persona'>")
                    .append("<div class='che-imagen-perfil' style=\"" + item.cssImagenPerfil + "\"></div>")
                    .append("<h5>" + item.nombreApellido + "<span>" + item.sector + "</span></h5>")
                    .appendTo(ul);
        }

        function filtrarPersonas(request, response) {
            var i, tokens;
            var resultado = [];
            var term = request.term.trim().toLowerCase();
            if (term) {
                term = che.ui.reemplazarAcentos(term);
                tokens = term.split(" ");
                for (i = 0; i < personasEmpresa.length; i++) {
                    if (contieneTodos(tokens, personasEmpresa[i].keywords)) {
                        resultado.push(personasEmpresa[i]);
                    }
                }
            }
            else {
                resultado = personasEmpresa;
            }
            response(resultado);
        }

        function contieneTodos(tokens, texto) {
            var i;
            for (i = 0; i < tokens.length; i++) {
                if (texto.indexOf(tokens[i]) === -1) {
                    return false;
                }
            }
            return true;
        }
    }

    function initBuscador() {
        init();

        function init() {
            $(".che-js-buscador").each(function() {
                var $input = $(this);
                var selector = $input.data("selector");
                prepararKeywordsParaBuscador(selector);
                $input.bind("keyup change paste cut", che.ui.throttle(function() {
                    filtrarRegistros($input, selector);
                }, 200));
                $input.focus();
            });
        }

        function prepararKeywordsParaBuscador(selector) {
            $(selector).each(function() {
                var $persona = $(this);
                var keywords = $persona.data("keywords");
                keywords = che.ui.reemplazarAcentos(keywords.trim().toLowerCase());
                $persona.data("keywords", keywords);
            });
        }

        function filtrarRegistros($input, selector) {
            var busqueda = $input.val();
            var tokens = che.ui.reemplazarAcentos(busqueda.trim().toLowerCase()).split(" ");
            $(selector).each(function() {
                var $persona = $(this);
                var keywords = $persona.data("keywords");
                if (contieneTodos(tokens, keywords)) {
                    $persona.removeClass("hide");
                }
                else {
                    $persona.addClass("hide");
                }
            });
        }

        /**
         * Indica si todos los string en el array tokens aparecen en alguna posicion
         * del texto.
         */
        function contieneTodos(tokens, texto) {
            var i;
            for (i = 0; i < tokens.length; i++) {
                if (texto.indexOf(tokens[i]) === -1) {
                    return false;
                }
            }
            return true;
        }

    }

    function initInfoPersonasExperiencia() {
        $(".che-js-info-persona-experiencia").each(function() {
            var $this = $(this);
            var nivel = $this.data("nivel");
            var xp = $this.data("xp");
            var xpProximoNivel = $this.data("xp-proximo-nivel");
            var estiloPopover = $this.data("xp-style");

            $this.popover({
                placement: "bottom",
                title: "Nivel: " + nivel,
                content: $.render.infoPersonas({
                    xp: xp,
                    xpProximoNivel: xpProximoNivel
                }),
                html: true,
                trigger: "hover",
                template: $.render.popover({style: estiloPopover || "light"})
            });
        });
    }

    function initHotkeys() {
        $(document).bind('keydown', 'ctrl+space', focusEnBuscadorGeneral);

        function focusEnBuscadorGeneral() {
            $("#che-buscador-personas-general").focus();
            $("#che-buscador-personas-general").select();
        }
    }
});
