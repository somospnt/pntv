pntv.ui.admin = (function () {

    var canal = $("#chetv-js-canal").val();
    function init() {
        listarAnuncios();
        bindearEventos();
    }

    function bindearEventos() {
        $('#chetv-js-guardar-anuncio').on('click', guardarAnuncio);
        $('#listadoAnuncios').on('click', '.chetv-js-eliminar-anuncio', eliminarAnuncio);
    }

    function guardarAnuncio() {
        var anuncio = {};
        var clave = $("#chetv-js-clave").val();
        var titulo = $("#chetv-js-titulo").val();
        var contenido1 = $("#chetv-js-contenido-1").val();
        var contenido2 = $("#chetv-js-contenido-2").val();
        var contenido3 = $("#chetv-js-contenido-3").val();
        var cron = $("#chetv-js-cron").val();
        var tipo = $("#chetv-js-tipo").val();
        var nivel = $("#chetv-js-nivel").val();
        var duracionMinutos = $("#chetv-js-duracion").val();

        anuncio.clave = clave === '' ? null : clave;
        anuncio.titulo = titulo;
        anuncio.contenido1 = contenido1;
        anuncio.contenido2 = contenido2;
        anuncio.contenido3 = contenido3;
        anuncio.cron = cron === '' ? null : cron;
        anuncio.tipo = tipo;
        anuncio.nivel = nivel;
        anuncio.duracionMinutos = duracionMinutos === '' ? null : duracionMinutos;

        pntv.service.anuncio.crearPorCanal(canal, anuncio)
                .done(function (anuncio) {
                    listarAnuncios();
                }).fail(pntv.ui.logError);
    }

    function eliminarAnuncio() {
        var $this = $(this);
        var clave = $this.data('clave');
        var canal = $this.data('canal');
        pntv.service.anuncio.eliminarPorCanalClave(canal, clave)
                .done(function () {
                    listarAnuncios();
                }).fail(pntv.ui.logError);
    }

    function listarAnuncios() {
        console.log("listaranuncios");
        pntv.service.anuncio.buscarPorCanal(canal)
                .done(function (anuncios) {
                    var $listadoAnuncios = $("#listadoAnuncios");
                    var $anuncioTablaTemplate = $("#anuncioTablaTemplate");
                    $listadoAnuncios.html($anuncioTablaTemplate.render(anuncios));
                })
                .fail(pntv.ui.logError);
    }

    return {
        init: init
    };

})();

$(document).ready(function () {
    pntv.ui.admin.init();
});
