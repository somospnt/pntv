pntv.ui.home = (function () {

    var DURACION_ANUNCIO_MS = 30000;
    var intervaloAnuncios = null;
    var intervaloActualizacion = null;
    var anunciosFijos = null;
    var canal = $("#fullpage").data('canal');

    function init() {
        later.date.localTime();
        refrescarAnuncios();
        configurarAutoupdate();
    }

    function configurarAutoupdate() {
        clearInterval(intervaloActualizacion);
        intervaloActualizacion = setInterval(refrescarAnuncios, 300000);
    }

    function refrescarAnuncios() {
        pntv.service.anuncio.buscarPorCanal(canal)
                .done(function (anuncios) {
                    anunciosFijos = anuncios.filter(function (anuncio) {
                        return (anuncio.cron == null);
                    });
                    mostrarAnuncios(anunciosFijos);
                    var anunciosACronear = anuncios.filter(function (anuncio) {
                        return (anuncio.cron != null);
                    });
                    programarAnuncios(anunciosACronear);
                })
                .fail(pntv.ui.logError);
    }

    function mostrarAnuncios(anuncios) {
        var $fullpage = $("#fullpage");
        $fullpage.empty();
        $fullpage.append($("#anuncioTemplate").render(anuncios, {
            colorNivelBg: colorNivelBg,
            colorNivelFg: colorNivelFg
        }));

        //destroying
        if (typeof $.fn.fullpage.destroy == 'function') {
            $.fn.fullpage.destroy('all');
        }

        $fullpage.fullpage({
            css3: true,
            continuousVertical: true,
            autoScrolling: true
        });
        window.clearInterval(intervaloAnuncios);
        intervaloAnuncios = setInterval(function () {
            $.fn.fullpage.moveSectionDown();
        }, DURACION_ANUNCIO_MS);
    }

    function programarAnuncios(anuncios) {
        $.each(anuncios, function (index, anuncio) {
            var cron = later.parse.cron(anuncio.cron);
            var ta = later.setInterval(function () {
                mostrar(anuncio);
            }, cron);
        });
    }

    function mostrar(anuncio) {
        var anuncios = [anuncio];
        mostrarAnuncios(anuncios);
        if (anuncio.duracionMinutos == null) {
            anuncio.duracionMinutos = 1;
        }
        setTimeout(mostrarAnuncios, anuncio.duracionMinutos * 60000, anunciosFijos);
    }

    function colorNivelBg(nivel) {
        switch (nivel) {
            case "INFO":
                return "#08b2e3";
            case "EXITO":
                return "#57a773";
            case "ALERTA":
                return "#fb8b24";
            case "ERROR":
                return "#ee6352";
            default:
                return "#484d6d";
        }
    }

    function colorNivelFg() {
        return "white";
    }

    return {
        init: init
    };
})();


$(document).ready(function () {
    pntv.ui.home.init();
});
