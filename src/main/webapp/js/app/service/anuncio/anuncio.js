pntv.service.anuncio = (function () {

    function buscarPorCanal(canal) {
        var url = pntv.service.url() + "canales/" + canal + "/anuncios";
        return pntv.service.get(url);
    }

    function crearPorCanal(canal, anuncio) {
        var url = pntv.service.url() + "canales/" + canal + "/anuncios";
        return pntv.service.put(url, anuncio);
    }

    function eliminarPorCanalClave(canal, clave) {
        var url = pntv.service.url() + "canales/" + canal + "/anuncios/" + clave;
        return pntv.service.eliminar(url);
    }

    return {
        buscarPorCanal: buscarPorCanal,
        crearPorCanal: crearPorCanal,
        eliminarPorCanalClave: eliminarPorCanalClave
    };
})();