<!DOCTYPE html>
<%@page pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html class="transition-navbar-scroll top-navbar-xlarge bottom-footer">
    <head>
        <c:set var="root" scope="request">${pageContext.request.contextPath}</c:set>
        <c:set var="url">${pageContext.request.requestURL}</c:set>
        <base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        
        <title>Che TV</title>
        <link rel="shortcut icon" type="image/png" href="${root}/favicon.ico"/>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    </head>

    <body>
        <tiles:insertAttribute name="body" />

        <script type="text/javascript" src="${root}/js/vendors/jquery/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script type="text/javascript" src="${root}/js/vendors/jsrender/jsrender.min.js"></script>

        <!-- Scripts de PNTv -->
        <script src="${root}/js/app/pntv.js"></script>
        <script src="${root}/js/app/service/service.js"></script>
        <script src="${root}/js/app/service/anuncio/anuncio.js"></script>
        <script src="${root}/js/app/ui/ui.js"></script>

        <tiles:importAttribute name="js" scope="page"/>
        <c:if test="${not empty js}">
            <script src="${root}/${js}"></script>
        </c:if>
    </body>
</html>
