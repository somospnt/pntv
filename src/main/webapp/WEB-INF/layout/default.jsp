<!DOCTYPE html>
<%@page pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html class="transition-navbar-scroll top-navbar-xlarge bottom-footer">
    <head>
        <c:set var="root" scope="request">${pageContext.request.contextPath}</c:set>
        <c:set var="url">${pageContext.request.requestURL}</c:set>
        <base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Che TV</title>
        
        <link rel="shortcut icon" type="image/png" href="${root}/favicon.ico"/>

        <link rel="stylesheet" type="text/css" href="${root}/css/vendors/fullpage/jquery.fullPage.css">
        <link rel="stylesheet" type="text/css" href="${root}/css/vendors/fullpage/examples.css">
        <link rel="stylesheet" type="text/css" href="${root}/css/vendors/fullpage/bootstrap.css">

        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" type="text/css" href="${root}/css/home.css">
    </head>

    <body>
        <tiles:insertAttribute name="body" />

        <!--JQuery-->
        <script type="text/javascript" src="${root}/js/vendors/jquery/jquery.min.js"></script>

        <!-- WebSockets -->
        <script type="text/javascript" src="${root}/js/vendors/sockjs/sockjs.min.js"></script>
        <script type="text/javascript" src="${root}/js/vendors/stomp/stomp.min.js"></script>

        <!--JSRender-->
        <script type="text/javascript" src="${root}/js/vendors/jsrender/jsrender.min.js"></script>

        <!--FullPage-->
        <script type="text/javascript" src="${root}/js/app/lib/fullpage.js/jquery.fullPage.min.js"></script>
        <script type="text/javascript" src="${root}/js/app/lib/fullpage.js/examples.js"></script>

        <!--Later JS-->
        <script type="text/javascript" src="${root}/js/vendors/laterjs/later.min.js"></script>

        <!-- Scripts de PNTv -->
        <script src="${root}/js/app/pntv.js"></script>
        <script src="${root}/js/app/service/service.js"></script>
        <script src="${root}/js/app/service/anuncio/anuncio.js"></script>

        <script src="${root}/js/app/ui/ui.js"></script>
        <tiles:importAttribute name="js" scope="page"/>
        <c:if test="${not empty js}">
            <script src="${root}/${js}"></script>
        </c:if>
    </body>
</html>
