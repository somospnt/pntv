<div class="container">
    <div class ="row">
        <div class="col-md-12">
            <h1>Nuevo anuncio en ${canal}</h1>
            <form class="form-horizontal">
                <input value="simple" type="hidden" id="chetv-js-tipo">
                <input value="${canal}" type="hidden" id="chetv-js-canal">
                
                <div class="form-group">
                    <label class="col-sm-2 control-label">Titulo</label>
                    <div class="col-sm-10">
                        <input class="form-control" value="" type="text" id="chetv-js-titulo" placeholder="El t�tulo principal a mostrar.">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Contenido 1</label>
                    <div class="col-sm-10">
                        <input class="form-control" value="" type="text" id="chetv-js-contenido-1" placeholder="(Opcional) Detalle adicional al t�tulo.">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Contenido 2</label>
                    <div class="col-sm-10">
                        <input class="form-control" value="" type="text" id="chetv-js-contenido-2" placeholder="(Opcional) Detalle adicional al t�tulo.">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Contenido 3</label>
                    <div class="col-sm-10">
                        <input class="form-control" value="" type="text" id="chetv-js-contenido-3" placeholder="(Opcional) Detalle adicional al t�tulo.">
                    </div>
                </div>                
                
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nivel</label>
                    <div class="col-sm-10">
                        <select class="form-control" id="chetv-js-nivel">
                            <option>INFO</option>
                            <option>EXITO</option>
                            <option>ALERTA</option>
                            <option>ERROR</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Cron <a href="http://crontab.guru/" target="_BLANK"><small>(?)</small></a></label>
                    <div class="col-sm-10">
                        <input class="form-control" value="" type="text" id="chetv-js-cron" placeholder="(Opcional) Una expresi�n cron para saber cu�ndo mostrar este anuncio. Si no se especifica, se muestra inmediatamente.">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Duracion (minutos)</label>
                    <div class="col-sm-10">
                        <input class="form-control" value="" type="text" id="chetv-js-duracion" placeholder="(Opcional) Si se especifica Cron, la cantidad de minutos por la cual se mostrar� el anuncio.">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-2 control-label">Clave</label>
                    <div class="col-sm-10">
                        <input class="form-control" value="" type="text" id="chetv-js-clave" placeholder="(Opcional) Una clave �nica para referenciar a este anuncio.">
                    </div>
                </div>                

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="button" class="btn btn-success" id="chetv-js-guardar-anuncio">Crear anuncio</button>
                    </div>
                </div>

            </form>            
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h2>Anuncios activos en ${canal}</h2>
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Clave</th>
                        <th>T�tulo</th>
                        <th>Contenido 1</th>
                        <th>Contenido 2</th>
                        <th>Contenido 3</th>
                        <th>Tipo</th>
                        <th>Nivel</th>
                        <th>Cron</th>
                        <th>Duraci�n</th>
                        <th>Acci�n</th>
                    </tr>
                </thead>
                <tbody id="listadoAnuncios">
                    <tr>
                        <td colspan="10"><i class="icon-time"></i> Cargando...</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script id ="anuncioTablaTemplate" type="text/html">
    <tr>
        <td>{{:clave}}</td>
        <td>{{:titulo}}</td>
        <td>{{:contenido1}}</td>
        <td>{{:contenido2}}</td>
        <td>{{:contenido3}}</td>
        <td>{{:tipo}}</td>
        <td>{{:nivel}}</td>
        <td>{{:cron}}</td>
        <td>{{:duracionMinutos}}</td>
        <td>
            <button data-canal="{{:canal}}" data-clave="{{:clave}}" class="btn btn-danger chetv-js-eliminar-anuncio">Eliminar</button>
        </td>
    </tr>
</script>

