<div id="fullpage" data-canal="${canal}">
</div>

<script id="anuncioTemplate" type="text/html">
    <div class="section" style="background-color: {{:~colorNivelBg(nivel)}}; color: {{:~colorNivelFg(nivel)}};">
        <div class="intro">
            <h1>{{:titulo}}</h1>
            <h2>{{:contenido1}}</h2>
            <h2>{{:contenido2}}</h2>
            <h2>{{:contenido3}}</h2>
        </div>
    </div>
</script>
