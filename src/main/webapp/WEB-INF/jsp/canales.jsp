<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Bienvenido a Che TV</h1>
            <ul>
                <c:forEach items="${canales}" var="canal">
                    <li>
                        <a href="${root}/canales/${canal}">Ver canal ${canal}</a>
                        -
                        <a href="${root}/canales/${canal}/admin">Admin</a>
                    </li>
                </c:forEach>
            </ul>
        </div>
    </div>
</div>