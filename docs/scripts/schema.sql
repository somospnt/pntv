-- -----------------------------------------------------------------------------
-- Dropear todas las tablas del esquema. Mas info: http://stackoverflow.com/questions/12403662/drop-all-tables
-- -----------------------------------------------------------------------------
SET FOREIGN_KEY_CHECKS = 0;
SET @tables = NULL;
SELECT GROUP_CONCAT(table_schema, '.', table_name) INTO @tables
  FROM information_schema.tables
  WHERE table_schema = 'pntv'; -- specify DB name here.

SET @tables = CONCAT('DROP TABLE ', @tables);
PREPARE stmt FROM @tables;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
SET FOREIGN_KEY_CHECKS = 1;

-- ----------------------------------------------------------------------------
-- Tablas
-- ----------------------------------------------------------------------------

CREATE TABLE anuncio (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    clave VARCHAR(255) NOT NULL,
    canal VARCHAR(50) NOT NULL,
    titulo VARCHAR(100) NOT NULL,
    tipo VARCHAR(50) NOT NULL,
    nivel VARCHAR(50) NOT NULL,
    cron VARCHAR(50),
    duracion_minutos INT,
    UNIQUE KEY unique_clave_canal (clave, canal)
);



CREATE TABLE anuncio_simple (
    id BIGINT PRIMARY KEY REFERENCES anuncio(id),
    contenido1 VARCHAR(100),
    contenido2 VARCHAR(100),
    contenido3 VARCHAR(100)
);
