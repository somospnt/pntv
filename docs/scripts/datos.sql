DELETE FROM anuncio_simple;
DELETE FROM anuncio;

-- -----------------------------------------------------------------------------
-- anuncios simples
-- -----------------------------------------------------------------------------
INSERT INTO anuncio
(id,    titulo,                 canal,      clave,      tipo,       nivel,      cron,           duracion_minutos) VALUES
(1,     'Bugs en prod',         'pnt',      'bugs',     'simple',   'ERROR',    '30 9 * * *',   30),
(2,     'OT pendientes',        'pnt',      'ot',       'simple',   'ALERTA',   null,           null),
(3,     'Mis Expensas',         'pnt',      'expensas', 'simple',   'EXITO',    null,           null),
(4,     'Hoy Techies',          'pnt',      'techies',  'simple',   'INFO',     null,           null);

INSERT INTO anuncio_simple
(id,    contenido1,         contenido2,         contenido3) VALUES
(1,     'QC 3435',          'QC 1111',          'QC 3434'),
(2,     'OT 1299',          null,               null),
(3,     '2802 UF',          '$3950 / mes',      '103 edificios'),
(4,     '15hs',             'Spring Data',      null);
