# PNTV #

Esta es una aplicación cuyo objetivo es servir de radiador de información. Esto se realiza mediante una única vista en donde se visualizan anuncios que van rotando secuencialmente. A su vez, expone un API para recibir anuncios de diferentes tipos. 

## API ##

**POST /api/canales/{mi_canal}/anuncios**

Defaults:

* nivel: "INFO"
* duracionMinutos: 1


Info adicional:

* serializar con Jackson: http://www.baeldung.com/jackson-inheritance
* JPA con herencia en varias tabla: https://en.wikibooks.org/wiki/Java_Persistence/Inheritance#Joined.2C_Multiple_Table_Inheritance
* Para configurar la expresión cron: http://crontab.guru



### Post simple de un anuncio (rota cada 60 segundos) ###

```
#!json

{
    "tipo": "simple",
    "titulo": "OT abiertas",
    "contenido1": "OT 1234 - OT 5678 - OT 3939"
}
```

### Post con nivel de un anuncio simple (rota cada 30 segundos)


```
#!json

{
    "titulo": "Desayuno",
    "nivel": "ALERTA",
    "titulo": "QC abiertos",
    "contenido1": "QC 2343"
}
```

### Post del PNT Diario

```
#!json

{
    "tipo": "simple",
    "titulo": "PNT Diario",
    "cron": "30 9 * * *",                    //empieza a las 9:30hs
    "duracionMinutos": 10                    //termina a las 9:40hs
}
```

### Post de Función Privada

```
#!json
{
    "tipo": "simple",
    "titulo": "Función Privada",
    "contenido1": "10hs - Sala Quinquela",
    "cron": "55 9 * * FRI",
    "duracionMinutos": 10 
}
```

### Post de carga de horas

```
#!json
{
    "tipo": "simple",
    "nivel": "ALERTA",
    "titulo": "Cargar las horas del mes",
    "contenido1": "(antes del mediodia por favor)",
    "cron": "*/30 10,16 20,29 * *",       //el 20 y el 29 de cada mes a las 10, 10:30, 16 y 16:30
    "duracionMinutos": 5 
}
```